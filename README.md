Provides:
1. A .cmd script that asks the user to reboot.  The user can decline, but if there
is no user input for a pre-configured duration, then the shutdown will commence.
2. A "Task Scheduler" .xml file that specifies when to launch the script.
    * set to start the windows_reboot_prompt.cmd if installed under `C:\local\windows-reboot-prompt\`
