::: Author: Nick Zwart
::: Date: 2016sep30
::: A reboot prompt that will execute if there is no user input.
::: Run this script with the following command:
:::     start /MAX windows_reboot_prompt.cmd

@echo off
:Ask
echo.
echo    Its %TIME% O`clock and this machine needs a reboot today, 
echo    would you like to reboot now? 
echo    (This machine will reboot in 60min if no input is given.) 
echo.

choice /d Y /t 3600
If %errorlevel% EQU 1 goto yes 
If %errorlevel% EQU 2 goto no

:yes
echo REBOOT
shutdown -f -r -c "Rebooting the system..."
goto :endofscript

:no
echo CANCEL
goto :endofscript

:endofscript
echo "Done."
exit /b 0
